\documentclass{scrartcl}
\usepackage[english]{babel}
\usepackage[margin=2.5cm,bottom=2.8cm]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{parskip}
\usepackage{xspace}
\usepackage[pdftex]{hyperref}
\usepackage[mark]{gitinfo2}
\usepackage[leftbars]{changebar}
\usepackage[symbol]{footmisc}
\usepackage[english]{varioref}

\newcommand{\versiontag}{ver.\gitAbbrevHash\ \ \gitAuthorDate}

\newcommand{\imagedir}{../images}

\newcommand{\mytitle}{Are we past the end of the world yet?\xspace}

\title{\mytitle\footnotemark[2]\\{\large (A pandemic pamphlet)}\\[-0.5\baselineskip]{\tiny (\versiontag)}}
\author{Nicola Bernardini}
\date{June 20 2020}

\setlength{\parskip}{0.618\baselineskip}
\setlength{\parindent}{0pt}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\begin{document}

\maketitle
\footnotetext[2]{\hspace{1mm}
\includegraphics[width=0.06\textwidth]{\imagedir/cc-by-sa}
\emph{\mytitle} (2020) is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
Based on a work at \url{https://gitlab.com/nicb/EOW-pamphlet}.
}

\renewcommand{\thefootnote}{\arabic{footnote}}
\setcounter{footnote}{0}

% Outline:
% vegan image
% analysis of the numbers: seem to be a bit high
% however, it is certainly a problem
% but: can this problem be resolved this way?
% - one kg of meat produces on average 2600 calories, so this meat provides
%   *at least* 328 billion kcal (assuming a conservative one kg of meat per
%   animal on average); how would we replace this loss? Wouldn't we desert the
%   whole earth to find enough sources of food?
%
\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.8541\textwidth]{\imagedir/40000-al-secondo}
  \end{center}
\end{figure}

Italy was recently plastered with this poster in giant formats\footnote{
  I'll provide a translation of the text on the right to the benefit of non--italian readers:
  \emph{We kill 40000 animals every second. These are the animals used to produce
  meat. If we tattooed one \emph{X} on our body for each animal, our bodies
  would be full of \emph{X}es. You can change your life in one second to save
  the life of others. Become a vegan.}%
}.

The message is strong and the number is indeed impressive -- and it led
immediately to some quick calculation. 40000 animals per second means 
% $1.26144~\times~10^{12}$ animals per year, that
means \emph{over 1 trillion (1000 billions)}
animals per year. Considering that the earth is over--crowded
by humans and we were ca. 7 \emph{billions} and 800 \emph{millions} last time
I looked (June 20, 2020)\footnote%
{%
  \url{https://www.worldometers.info/}
},%
if this number was realistic there would be some 162 animals killed \emph{per
person} (including babies, elderly, etc.) per year; this makes one
animal each other day for each human being -- which looks a bit over--estimated. I have no real
way of verifying such claims, so I won't go into further considerations about
them. Whether they are grounded on truth or not, the problem they wish to
address is much larger than the figures themselves.

Say we would like to adopt the vegan strategy and refuse to kill the 1
trillion animals. For simplicity, let's equate each animal to a kilogram of
meat: some are going to be somewhat less, but many are going to be way more,
so the estimate figure is to be considered conservative. Since one kilogram of
meat produces on average 2600 kilocalories, we will need to replace about 1
trillion proteins for over 3 thousands of trillions of kilocalories. What
vegan edible source of energy would be able to provide such an enormous quantity? We
would probably end up deserting the whole earth in the process.

% reasoning:
% - being carnivorous helped us evolve into sapiens (Morris)

There is another issue connected to this: as Desmond Morris pointed out in his
\emph{The Naked Ape} (\cite{morris1967naked}), the chase of the \emph{homo erectus} from the
forest into the savannah perpetrated by other stronger primates has had the
direct consequence of leading the hominids to become carnivorous to survive
the lack of vegetable food of their new environment. This change has led
to collect enough energy to gather some free time from actually looking for
food (something that was a full--time endeavor in their prior collector--only
social groups). This free time was used to develop the frontal cortex,
thought, logical capabilities, language, etc. Though Desmond Morris' theories
have been widely criticized (cf. for example \cite{rutherford2018brief} or \cite{saini2017inferior}),
it is a fact that the human species has evolved
from \emph{homo erectus} into \emph{homo sapiens} and, as Yuval Harari has
recently shown (\cite{harari2014}), this change has led this species to become the undisputed
master of the world.

% - capability of handling complexity is balanced by longer and longer
%   childhood (learning period)

However, complex brain capabilities come at a cost, and this can be identified
with the longer and longer time needed for the \emph{sapiens} breed to grow up. 
This implied in turn the necessity of developing all the affective and
emotional functions of the brain -- with all the implications that ensued:
% - in order to support this longer period, our brain has developed emotions
% - our emotional system is behind ethics and morals
ethics and morals are near descendants to those affective and emotional functions.

% - our ethics and morals build everything else: enlightenment, scientific
%   method, progress

And it is indeed out of discussion that all our more recent progresses, such as the enlightenment, the scientific
method and the overwhelming technological development are so strongly connected
to emotional functions, ethics and morals.

% - efficiency and abundancy of nutrition produces demographic growth

Steven Pinker (\cite{pinker:EN2018})
has convincingly made the case for grounding the progressive principles of
our current society in the innovative ideas proposed by the
\emph{enlightenment} period. He has triumphantly shown that the modern human
societies have been able to produce an immense demographic growth
(as shown in \vref{fig:growth}).

\begin{figure}[h]
  \includegraphics[width=0.8541\textwidth]{\imagedir/demographic_growth}
  \caption{\label{fig:growth} Demographic growth of the past 10000 years}
\end{figure}

% - in turn, these achievements make us think that we cannot leave anybody nor
%   anything behind: humans, animals are all equal and should be treated
%   equally
% - some data: demographic growth, speed of growth (worldometer page)

Furthermore, affection combined with ethic and morals have led us to think
that no human or animal can be \emph{left behind} in this progressive
narrative. We care for humans and animals, from baby born to elderly, and we will
do our best to keep them safe and sound. Our affective schemas are so deeply
rooted within our \emph{weltanschauung}, no matter where we live in the
world or what ideology we embrace, that it is impossible for us to get rid of
them. In fact, the very thought of getting rid of our emotional empathy is
considered obscene and disgusting -- so much so that we consider nature to be
\emph{cruel} when it tries to keep an evolutionary balance within its
complicated ecosystems.

% - however: this is not feasible (at least forever) ad infinitum, because it
%   requires infinite resources growing at an exponential rate

However, just a cursory glance at the figure \vref{fig:growth} should make it
very very clear that this growth rate, connected to the \emph{sapiens} ability
to provide means to pursue its own moral and ethical obligations, will be
impossible to be maintaned forever.
In fact, there is no need to look at the chart: we are right in the middle of
a world--wide pandemic which has closely followed (within 20 years) another
two smaller ones, the earth is heating up at an unreasonable pace due to the
\emph{sapiens} impact and we are quickly using
up all energy resources (animals, vegetation, water, oil, gas, you name it).
This crazy downhill race towards an inevitable implosion is quite evident by
now. What is less evident is that it is intrinsically connected to our
emotional and affective world.

% - mention ``decroissance sereine'' Latouche

Of course, there have been some philosophical attempts to analyze this
situation and find appropriate solutions to the issues it creates.
The most notable one is the \emph{d\'ecroissance s\'ereine} (degrowth) theory by french
economist Serge Latouche (\cite{latouche2006pari}),
created from an earlier theorization of Nicholas Georgescu--Roegen
(\cite{georgescu1995decroissance}).

% brief summary description of Latouche view
According to Latouche, the narrative of positive development is falling short
in this finite world and we must subvert the indicators of wealth by changing
our priorities from quantitative to qualitative categories.
We need to redistribute the world wealth in a more rational and equitable
fashion, placing the quality of lives before any material wealth indicator.
% - question: is this feasible? How do we explain to developing countries,
%   which we have exploited up to today?

However sympathetic we may be to Latouche's utopias,
we cannot avoid noticing a couple of problems that will affect negatively
their validity.
First of all, Latouche implies that one or more solutions are available,
while it is actually possible, in fact, that no solution is really attainable.
We are all \emph{urged} to find solutions to our problems,
but what if these solutions contradict blatantly our affective imperatives?
The second problem is constituted by the very fact that this advanced and visionary philosophical point of view
is born in France, one of the top developed countries in the world who has
reached its level of wealth and growth through its shady past of predatory
exploitation of its colonies (of course it is by no means the only one:
\emph{all} top developed countries have shared such a past and our present --
with a host of \emph{developing countries} which are thriving to reach the
\emph{first world} -- is a direct consequence of that past).

Now that our affective imperatives have pushed our civilization to become somewhat more
equitable and right by giving up colonies, slavery, and depredation,
how are we going to propose to those very same \emph{developing countries} the idea that
they should abandon their ambition to become like us -- simply because
\emph{we} were wrong and \emph{our} level of wealth is not sustainable for
ourselves let alone the rest of the world?

% - furthermore: could we stop the research on longevity, tagging it as
%   'unethical'? should we rather concentrate on arriving at a healthy death?
%   how can we do such thinking when we care so much for our elderly who make
%   us so happy by living past 100 years?

The issues do not stop with wealth.
Another good example is the research on longevity and the obsession of the
first world with fitness and health. Would we be able to tag as
\emph{unethical} such research domains, simply because they are plainly
anti--ecological? Could we propose that, rather than extending life to 120+
years, a better endeavor would be to reach death being healthy and in full
possession of our cognitive abilities?
But then again: wouldn't it be cruel to die if we are still so perfectly fit?
After all, it often happens that life quality degradation due to age makes the
death of our beloved ones more acceptable, isn't it?

Which takes us back to the initial wailing call to become vegans to save
trillions of animals -- probably deserting earth and then letting the
above--mentioned animals and ourselves die of famine.

%
% Conclusion:

\section*{Conclusive dilemma}

The arguments above propose a single, gigantic, dilemma which is generated by
a cognitive bias created in turn by the ethical and moral views of our
emotional and affective mind.

Aren't our emotions and affections profoundly unnatural and anti-ecological?

After all, when we refer to the \emph{cruelty} of nature we are projecting
our morality onto it, reading it through the narrow cognitive bias that
plagues our reasoning. Nature is obviously not cruel -- it doesn't even
consider the category of cruelty or benignity, for that matter. In fact,
our planet seems to be a real extraordinary \emph{unicum} in the universe in
terms of evolutionary balance. We could even consider it a perfect system,
if \emph{sapiens} wouldn't have taken over it.

% - perhaps the end of the world was inscribed already in our DNA when homo
%   became sapiens. In evolutionary terms, sapiens are too powerful and
%   overtake evolution in ways that are not compatible with ``natural speeds''.
%   Killing of animals, global warming, pollution, pandemics are all signs of
%   this problem. Quote worldometer future and see that it is not easiliy
%   solved. Perhaps our only way out is to be conscious about it, and not
%   strive to live longer, but to live better.

Which leads us to the last consideration: perhaps the end of the world was
already inscribed in our DNA when the \emph{homo erectus} became
\emph{sapiens}. In evolutionary terms, \emph{sapiens} are way too powerful and
will continuously strive to overtake nature and evolution in ways that are not
compatible with sustainability.
Killing of animals, global warming, pollution, pandemics are all evident
outcomes of this problem.
If you give a look at the
\href{https://worldometer.info}{worldometer front page}
you will have an immediate and devastating perception of how hard is this
problem to solve.
Perhaps our only way out is to use our consciousness to grasp it,
and not strive to live longer, but just as best as we can in the most
equitable and egalitarian way.

% - isn't that the rethoric used by the poster itself the very same
%   ``progressive'' one that is leading us to catastrophe? (attractive man, strong
%   body, tattoos are trendy)

As a post-scriptum, allow me to stress that the graphic and textual language
used by the poster shown in the beginning (attractive man, strong body of a
clearly (ex--)carnivorous specimen, trendy
tattoos -- all features obviously used to attract our attention) uses the same
marketing strategies that are speeding up our rush towards catastrophe.
What kind of solution can we expect from them?

\bibliographystyle{apalike}
\bibliography{../EOW}

\end{document}
