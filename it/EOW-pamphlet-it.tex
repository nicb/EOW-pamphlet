\documentclass{scrartcl}
\usepackage[italian]{babel}
\usepackage[margin=2.5cm,bottom=2.8cm]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{parskip}
\usepackage{xspace}
\usepackage[pdftex]{hyperref}
\usepackage[mark]{gitinfo2}
\usepackage[leftbars]{changebar}
\usepackage[symbol]{footmisc}
\usepackage[italian]{varioref}

\newcommand{\versiontag}{ver.\gitAbbrevHash\ \ \gitAuthorDate}

\newcommand{\imagedir}{../images}

\newcommand{\mytitle}{\`E gi\`a passata la fine del mondo?\xspace}

\title{\mytitle\footnotemark[2]\\{\large (Pamphlet pandemico)}\\[-0.5\baselineskip]{\tiny (\versiontag)}}
\author{Nicola Bernardini}
\date{Giugno 2020}

\setlength{\parskip}{0.618\baselineskip}
\setlength{\parindent}{0pt}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\begin{document}

\maketitle
\footnotetext[2]{\hspace{1mm}
\includegraphics[width=0.06\textwidth]{\imagedir/cc-by-sa}
\emph{\mytitle} (2020) \`e sottoposto alla licenza Creative Commons Attribution-ShareAlike 4.0 International License.
Le sorgenti di questo lavoro sono su \url{https://gitlab.com/nicb/EOW-pamphlet}.
}

\renewcommand{\thefootnote}{\arabic{footnote}}
\setcounter{footnote}{0}

% Outline:
% vegan image
% analysis of the numbers: seem to be a bit high
% however, it is certainly a problem
% but: can this problem be resolved this way?
% - one kg of meat produces on average 2600 calories, so this meat provides
%   *at least* 328 billion kcal (assuming a conservative one kg of meat per
%   animal on average); how would we replace this loss? Wouldn't we desert the
%   whole earth to find enough sources of food?
%
\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.8541\textwidth]{\imagedir/40000-al-secondo}
  \end{center}
\end{figure}

L'altro giorno l'Italia si \`e svegliata tappezzata con questi manifesti in
formato gigante.

Il messaggio \`e forte e il numero fa certamente impressione -- e porta subito
a fare qualche calcolo aggiuntivo.
% $1.26144~\times~10^{12}$ animals per year, that
40000 animali al secondo significa \emph{pi\`u di mille miliardi}
di animali all'anno.
Considerando che la terra \`e gi\`a sovra--popolata di esseri umani
(l'ultima volta che ho guardato le
stime il 20 giugno 2020 esse indicavano poco meno di 7 \emph{miliardi} e 800 \emph{milioni})\footnote%
{%
  \url{https://www.worldometers.info/}
},%
se la cifra sopracitata fosse realistica ci\`o significherebbe
che vengono uccisi in media 162 animali all'anno
per \emph{ogni essere umano} sulla terra (inclusi vecchi, bambini, ecc.).
Vale a dire praticamente un animale ogni due giorni per ciascuno, quantit\`a che appare
forse sovrastimata.
Non avendo per\`o modo di verificarla la dar\`o per fondata;
anche perch\'e, veritiera o meno, il problema che sottolinea \`e
comunque enorme.

Facciamo l'ipotesi di adottare la strategia vegana rifiutandoci globalmente
di uccidere i mille miliardi di animali. Per semplicit\`a, equipariamo ogni
animale a un chilo di carne macellata: alcuni animali ne produrranno meno, ma
molti ne produrranno molta di pi\`u quindi possiamo considerare questa stima
conservativa. Un chilo di carne produce in media 2600 kilocalorie, quindi
dovremo trovare il sostituto vegano di mille miliardi di proteine per oltre
tremila miliardi di miliardi di kilocalorie. 
Quale sorgente vegetale di energia potrebbe produrre questa quantit\`a
abnorme? Probabilmente desertificheremmo la terra nel tentativo di
procurarcela.

% reasoning:
% - being carnivorous helped us evolve into sapiens (Morris)

C'\`e un corollario. Nel 1967 Desmond Morris ci raccontava nel suo \emph{La
Scimmia Nuda} (\cite{morris1967naked}) della cacciata del \emph{homo erectus}
dalla foresta ad opera di primati ben pi\`u forti di quest'ultimo.
La scarsit\`a di vegetazione della savana, punto di approdo della fuga dei
nostri antenati, ebbe la conseguenza diretta di costringere gli ominidi a
diventare carnivori per sopravvivere. Il cambio di alimentazione
li ha portati ad accumulare energia pi\`u rapidamente -- lasciando loro pi\`u
tempo libero rispetto all'incessante attivit\`a di ricerca di cibo dei loro
antenati esclusivamente raccoglitori. Questo nuovo tempo ``per pensare'' li ha
condotti, a sua volta, a sviluppare la corteccia frontale del cervello, e
via via il pensiero, le capacit\`a logiche, il linguaggio, ecc.
Le teorie di Morris sono state sottoposte a un severo scrutinio
e la loro validit\`a \`e stata pi\`u volte messa in dubbio
(si vedano ad es. \cite{rutherford2018brief} or \cite{saini2017inferior}),
ma l'evoluzione da \emph{homo erectus} a \emph{homo sapiens} \`e un dato di
fatto e, come ha recentemente mostrato Yuval Harari (\cite{harari2014}),
questo cambiamento ha portato gli esseri umani al dominio assoluto sulla
catena alimentare.

% - capability of handling complexity is balanced by longer and longer
%   childhood (learning period)

Tuttavia, le accresciute capacit\`a cerebrali hanno un costo non irrilevante,
identificabile col tempo straordinariamente lungo di gestazione e poi di
crescita delle nidiate di \emph{sapiens}.
A sua volta, la lentezza di questo processo ha richiesto lo sviluppo di tutte
le funzioni emotive e affettive del cervello per riuscire a creare la tensione
psicologica necessaria a resistere a tutte le difficolt\`a del caso,
con tutte le implicazioni conseguenti:
i nostri imperativi morali ed etici discendono direttamente dalle nostre
funzioni emotive e affettive.

% - our ethics and morals build everything else: enlightenment, scientific
%   method, progress

Da questi ai nostri progressi pi\`u recenti il passo \`e breve:
l'illuminismo, il metodo scientifico e lo sviluppo tecnologico forsennato
degli ultimi decenni sono tutti connessi strettamente alle nostre
funzioni emotive, alle nostre etiche e alle nostre morali.

% - efficiency and abundancy of nutrition produces demographic growth

In \emph{Illuminismo adesso},
Steven Pinker (\cite{pinker:EN2018})
ha illustrato in maniera convincente la fondazione dei principi progressisti
delle nostre societ\`a attuali nelle idee innovative e diverse
proposte dall'illuminismo.
Egli ha mostrato -- forse con un pizzico di trionfalismo ingenuo --
che le societ\`a moderne sono state in grado,
in questo modo, di produrre una crescita demografica immensa
(illustrata in \vref{fig:growth}).

\begin{figure}[h]
  \includegraphics[width=0.8541\textwidth]{\imagedir/demographic_growth}
  \caption{\label{fig:growth} Demographic growth of the past 10000 years}
\end{figure}

% - in turn, these achievements make us think that we cannot leave anybody nor
%   anything behind: humans, animals are all equal and should be treated
%   equally
% - some data: demographic growth, speed of growth (worldometer page)

Inoltre, la combinazione di affetti, etica e morale ci ha convinto
che nessun umano o animale debba essere \emph{lasciato indietro}
in questa narrazione progressista. Abbiamo a cuore umani e animali,
dalla nascita sino alla vecchiaia, e siamo pronti a fare di tutto per
garantire la loro salute e sicurezza. I nostri schemi affettivi
sono cos\`i radicati nella nostra visione del mondo,
quali che siano le nostre origini, le nostre religioni e le ideologie che
abbracciamo, che \`e per noi impossibile liberarcene.
Anzi, consideriamo disgustosa e rivoltante la sola idea di liberarci
della nostra empatia emotiva verso gli altri --
tanto da considerare \emph{crudele} la natura quando essa tenta di mantenere
un equilibrio evolutivo all'interno dei suoi ecosistemi complicati.

% - however: this is not feasible (at least forever) ad infinitum, because it
%   requires infinite resources growing at an exponential rate

Tuttavia, anche uno sguardo superficiale alla figura \vref{fig:growth}
dovrebbe chiarire senza ombra di dubbio che il mantenimento di questo tasso di crescita,
legato all'abilit\`a dei \emph{sapiens} di costruire i mezzi per poter
perseguire i propri obblighi morali ed etici, sar\`a impossibile nel medio e
nel lungo termine.
Non \`e nemmeno necessario guardare il grafico:
siamo nel bel mezzo di una pandem\`ia mondiale che \`e la terza in una ventina
d'anni, la terra si sta surriscaldando a velocit\`a non consone
all'adattivit\`a della natura a causa delle attivit\`a dei soli \emph{sapiens}
i quali stanno oltretutto consumando tutte le risorse energetiche
(animali, vegetazione, acqua, petrolio, gas, ecc.).
Questa folle discesa verso l'inevitabile implosione finale \`e ormai pi\`u
che evidente. Forse \`e meno chiaro che essa sia intrinsecamente connessa al
nostro mondo emotivo e affettivo.

% - mention ``decroissance sereine'' Latouche

Naturalmente non sono mancati i tentativi di analisi filosofica di questa
situazione al fine di trovare soluzioni appropriate ai problemi che essa crea.
Quello pi\`u incisivo \`e forse stata la teoria della \emph{d\'ecroissance
s\'ereine} (la \emph{decrescita felice})
dell'economista francese Serge Latouche (\cite{latouche2006pari}),
fondata sul precedente pensiero elaborato da Nicholas Georgescu--Roegen
(\cite{georgescu1995decroissance}).

% brief summary description of Latouche view

Secondo Latouche, la narrazione dello sviluppo positivo \`e impossibile da
perseguire in un mondo finito e dobbiamo sovvertire i nostri indicatori di
ricchezza modificando le nostre priorit\`a da categorie quantitative a
qualitative.
Dobbiamo ridistribuire la ricchezza del mondo in maniera pi\`u razionale ed
equa, collocando la qualit\`a delle vite umane davanti a qualsiasi indicatore
di ricchezza materiale.
% - question: is this feasible? How do we explain to developing countries,
%   which we have exploited up to today?

Per quanto possiamo apprezzare le idee utopiche di Latouche,
non possiamo fare a meno di notare alcuni problemi che le rendono
sostanzialmente inutilizzabili.
Il primo tra questi \`e quello di dare per scontato -- in una tipica logica
progressista che \`e in contraddizione con l'osservazione stessa --
che \emph{esista una soluzione} a queste problematiche.
\`E invece piuttosto probabile che una soluzione non possa esistere perch\'e essa
entrerebbe inevitabilmente in conflitto con i nostri irrinunciabili principi etici e morali.
Il secondo \`e che il punto di vista filosofico avanzato e visionario di Latouche 
sia sorto in Francia, una delle nazioni pi\`u potenti del mondo che
ha raggiunto il proprio livello di benessere e di crescita
attraverso un discutibile passato di sfruttamento predatorio
delle proprie colonie (naturalmente la Francia non \`e sola in questo triste
primato: \emph{tutte} le nazioni pi\`u progredite del mondo hanno condiviso
un simile passato e il nostro presente -- costituito da un gruppone di
\emph{paesi in via di sviluppo} che sta cercando di raggiungere il cosiddetto
\emph{primo mondo} -- \`e una conseguenza diretta di \emph{quel} passato).

Ora che i nostri imperativi affettivi hanno costretto le nostre civilit\`a a
diventare pi\`u solidali, eque e giuste eliminando colonie, schiavit\`u e sfruttamento predatorio,
come potremmo proporre a quegli stessi \emph{paesi in via di sviluppo}
l'idea che dovrebbero abbandonare la loro ambizione di ergersi al nostro
livello -- semplicemente perch\'e \emph{noi} abbiamo capito di aver sbagliato
e che il \emph{nostro} livello di ricchezza non \`e sostenibile n\'e per noi
n\'e tantomeno per loro?

% - furthermore: could we stop the research on longevity, tagging it as
%   'unethical'? should we rather concentrate on arriving at a healthy death?
%   how can we do such thinking when we care so much for our elderly who make
%   us so happy by living past 100 years?

I problemi non si fermano certo solo alla ricchezza.
Un altro esempio appropriato \`e la ricerca sulla longevit\`a e l'ossessione
del primo mondo con la salute e la \emph{fitness}.
Potremmo etichettare come \emph{poco etici} questi ambiti di ricerca,
semplicemente perch\'e sono genericamente anti--ecologici?
Potremmo proporre che,
invece di cercare di estendere la vita oltre i 120 anni,
un obiettivo pi\`u centrato sarebbe quello di raggiungere la morte
nel pieno possesso delle nostre abilit\`a fisiche e intellettuali?
Ma poi: non sarebbe forse crudele morire in piena salute fisica e mentale?
La morte non ci \`e pi\`u accettabile quando tutte le nostre capacit\`a sono
degenerate? Come si pu\`o vedere, ogni soluzione crea altri problemi.

Il che ci riporta al lancinante appello dell'inizio per diventare vegani al
fine di salvare migliaia di miliardi di animali -- desertificando la terra nel
processo e quindi poi uccidendo noi stessi e gli animali sopracitati con
le inevitabili carestie.

%
% Conclusion:

\section*{Dilemma conclusivo}

Gli argomenti sopracitati propongono tutti un singolo dilemma gigante,
generato dal \emph{bias cognitivo} creato dalle nostre vedute etico--morali
prodotte a loro volta dal sistema affettivo--emotivo del nostro cervello.

E se le emozioni e gli affetti fossero profondamente
contro--natura e anti--ecologici?

Dopo tutto, quando ci riferiamo alla \emph{crudelt\`a} della natura
non facciamo altro che proiettare la nostra visione morale su di essa,
leggendo la sua azione attraverso il ristretto \emph{bias cognitivo} che
inficia il nostro modo di ragionare. \`E ovvio che la natura non possa essere
crudele -- in essa non \`e possibile ravvisare n\'e la categoria della
crudelt\`a n\'e quella della benignit\`a.
Piuttosto,
se consideriamo il suo equilibrio evolutivo,
il nostro pianeta sembra essere un vero \emph{unicum}
extra--ordinario nell'universo.
Potremmo quasi considerarlo perfetto, se gli \emph{erecti} non fossero
diventati \emph{sapiens} tentando di appropriarsi di esso.

% - perhaps the end of the world was inscribed already in our DNA when homo
%   became sapiens. In evolutionary terms, sapiens are too powerful and
%   overtake evolution in ways that are not compatible with ``natural speeds''.
%   Killing of animals, global warming, pollution, pandemics are all signs of
%   this problem. Quote worldometer future and see that it is not easiliy
%   solved. Perhaps our only way out is to be conscious about it, and not
%   strive to live longer, but to live better.

Il che ci conduce all'ultima considerazione:
forse la fine del mondo \`e scritta nel DNA dei \emph{sapiens}.
In termini evolutivi, i \emph{sapiens} sono troppo potenti e lotteranno
continuamente per sopraffare la natura e l'evoluzione in modi che non sono
compatibili con la sostenibilit\`a.
L'uccisione di animali, il riscaldamento globale, l'inquinamento, le
pandem\`ie sono solo i segnali pi\`u esteriori di questo problema.
Basta dare un'occhiata alla
\href{https://worldometers.info}{pagina principale del sito \emph{worldometers}}
per avere una percezione immediata e devastante di quale sia la sua
dimensione.
L'unica possibilit\`a che vedo non \`e quella di cercare una soluzione,
quanto quella di accettare la fine cercando di arrivarci
nel modo pi\`u equo ed ugualitario possibile.

% - isn't that the rethoric used by the poster itself the very same
%   ``progressive'' one that is leading us to catastrophe? (attractive man, strong
%   body, tattoos are trendy)

In guisa di post--scriptum finale, permettetemi di sottolineare che il
linguaggio testuale e grafico utilizzato dal poster mostrato in apertura
(bell'uomo, muscoloso quanto solo un (ex--)carnivoro pu\`o essere,
\emph{tattoo} alla moda -- il tutto naturalmente
utilizzato con successo per attrarre la nostra attenzione)
usa le stesse strategie di marketing che accelerano la nostra corsa verso la
catastrofe. Che salvezza possiamo aspettarci in questo modo?

\bibliographystyle{apalike}
\bibliography{../EOW}

\end{document}
