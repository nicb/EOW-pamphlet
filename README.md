# **Are we past the End of the World yet?** (a pandemic pamphlet)

These are the *LaTeX* sources of the *Are we past the End of the World yet?* pamphlet.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">EOW-pamphlet</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/nicb/EOW-pamphlet" rel="dct:source">https://gitlab.com/nicb/EOW-pamphlet</a>.
[Full text license](./LICENSE)
