%
%
sph=3600;       % seconds per hour
hpd=24;         % hours per day
dpy=365;        % days on a normal year
pop=7792686640; % world population on June 20, 2020 at 20:23 CEST
cpkgm=2600;     % calories per kilo of meat

aps=40000;             % animals killed per second
apy=aps*sph*hpd*dpy;   % animals killed per year
appop=apy/pop;         % animals killed per year per person
apyw=apy/10.0e3;       % weight of animals if they all weighted 1 kg
tppbm=apy*0.8;        % total proteins provided by meat (assuming 20% fat)
tcpbm=apy*cpkgm;      % total calories provided by meat (average)

printf("Animals killed per year            %13g\n", apy);
printf("Animals killed per year per person %8.2f\n", appop);
printf("If the animals would all weight 1 kg the total weight would be %ld tons\n", apyw);
printf("If the animals would all weight 1 kg the total protein amount provided would be %g kg (%g kcal)\n", tppbm, tcpbm);
